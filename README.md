# README #


The app is quite simple. It just display a single screen with a list of images. 
The architecture is based in the 3 tier: app plus 2 modules, domain and data. 
Some of the main technologies used are:
* Kotlin
* Dagger 2 
* RxJava2
* Retrofit 2
* Room
* LiveData
* ViewModel
* JUnit with Mockito

The design is an MVP. 
The flow is:
GalleryActivity will request some images from Flickr api using the presenter, that will pass the request to the interactor and will load the data from 
the repository. Once the data is available, it will be mapped to our custom model for the app: a simple list with the images, and it will
be cached in Room. When data is stored in Room, the observer of the liveData will triggered and the images will be displayed.


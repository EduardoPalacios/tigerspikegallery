package com.tigerspike.domain

import com.nhaarman.mockito_kotlin.*
import com.tigerspike.domain.interactors.GalleryInteractor
import com.tigerspike.domain.interactors.GalleryInteractorImpl
import com.tigerspike.domain.mappers.FlickrResponseMapper
import com.tigerspike.domain.models.api.FlickrResponse
import com.tigerspike.domain.models.api.Item
import com.tigerspike.domain.models.api.Media
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks

class GalleryInteractorTest {

    private val interactor: GalleryInteractor by lazy { GalleryInteractorImpl(provider, mapper) }
    private val mapper = FlickrResponseMapper()
    private val items = listOf(Item("title", "dateTaken", Media("imageUrl")))

    @Mock lateinit var provider: GalleryProvider
    @Mock lateinit var callback: OnErrorCallback

    @Before
    fun setUp() {
        initMocks(this)
    }

    @Test
    fun shouldRequestFlickrImages() {
        whenever(provider.request()).thenReturn(Single.just(FlickrResponse(items)))
        interactor.requestImages(callback)
        verify(provider, times(1)).request()
        verify(provider, times(1)).cache(any())
    }


    @Test
    fun shouldCacheListOfImages() {
        whenever(provider.request()).thenReturn(Single.just(FlickrResponse(items)))
        doAnswer {
            assertThat(it.arguments.size, `is`(1))
        }.whenever(provider).cache(listOf("imageUrl"))
        interactor.requestImages(callback)
    }

    @Test
    fun shouldHandleEmptyContent() {
        whenever(provider.request()).thenReturn(Single.just(FlickrResponse()))
        interactor.requestImages(callback)
        verify(provider, times(1)).request()
        verify(callback, times(1)).onNoContent()
    }

    @Test
    fun shouldReceiveAnError() {
        whenever(provider.request()).thenReturn(Single.error(Exception("Error!")))
        interactor.requestImages(callback)
        verify(provider, times(1)).request()
        verify(callback, times(1)).onError()
    }

    @Test
    fun shouldFailWithTestObserverError() {
        whenever(provider.request()).thenReturn(Single.error(Exception("Error!")))
        val testObserver = getTestObserver()
        testObserver.assertError { t: Throwable -> t.message.equals("Error!") }
    }

    private fun getTestObserver(): TestObserver<FlickrResponse> {
        val testObserver = provider.request().test()
        testObserver.awaitTerminalEvent()
        return testObserver
    }
}
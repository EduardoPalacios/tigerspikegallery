package com.tigerspike.domain.mappers

import com.google.gson.Gson
import com.tigerspike.domain.models.api.FlickrResponse
import com.tigerspike.domain.models.api.Item
import com.tigerspike.domain.models.api.Media
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test
import java.io.InputStreamReader

class FlickrResponseMapperTest {

    private val mapper = FlickrResponseMapper()

    @Test
    fun shouldHandleNullableContentInResponse() {
        val actual = mapper.map(FlickrResponse())
        assertThat(actual.size, `is`(0))
    }

    @Test
    fun shouldHandleNullableItems() {
        val actual = mapper.map(FlickrResponse(listOf(Item(null), Item(null))))
        assertThat(actual.size, `is`(0))
    }

    @Test
    fun shouldReturnListOfImageUrls() {
        val actual = mapper.map(FlickrResponse(listOf(
                Item("title1", "dateTaken1", Media("imageUrls1")),
                Item("title2", "dateTaken2", Media("imageUrls2")),
                Item("title2", "dateTaken2", Media("imageUrls2")),
                Item("title3", "dateTaken3", Media("imageUrls3"))))
        )
        assertThat(actual.size, `is`(3))
    }

    @Test
    fun shouldReturnSortedListOfImageUrls() {
        val actual = mapper.map(FlickrResponse(listOf(
                Item("title1", "dateTaken1", Media("imageUrls1")),
                Item("title2", "dateTaken2", Media("imageUrls2")),
                Item("title2", "dateTaken2", Media("imageUrls2")),
                Item("title4", "dateTaken4", Media("imageUrls4")),
                Item("title3", "dateTaken3", Media("imageUrls3"))))
        )
        val expected = listOf("imageUrls1", "imageUrls2", "imageUrls3", "imageUrls4")
        assertThat(actual, `is`(expected))
    }

    /**
     * Flickr generate responses that contains the same images under different users, most of
     * those images seems to be kind of spam and naughty content.
     * This test ensures we remove those duplicates.
     */
    @Test
    fun shouldFindAndRemoveDuplicatesInResponseItems() {
        val asset = javaClass.classLoader.getResourceAsStream("flickr_response.json")
        val reader = InputStreamReader(asset, "UTF-8")
        val response = Gson().fromJson(reader, FlickrResponse::class.java)
        val actual = mapper.map(response)
        assertThat(actual.size, `is`(18))
    }


}
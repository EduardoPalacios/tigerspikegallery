package com.tigerspike.domain.mappers

import com.tigerspike.domain.models.api.FlickrResponse
import javax.inject.Inject

class FlickrResponseMapper @Inject constructor() {

    /**
     * Some users in Flickr has the same pictures under different accounts.
     * This images are kind of naughty stuff and sometimes comes 1 to N times in the response.
     * This method tries to filter that in order to avoid duplicates, which are quite tricky
     * since the duplicated images, even if they have the same title, don't have the same user
     * or date when image was taken, because of that i had to check in the description :(
     */
    fun map(response: FlickrResponse): List<String> = response.items
            ?.distinctBy { it.title to it.description?.split("rel=")?.last() }
            ?.mapNotNull { it.media?.mediaLink }
            ?.sortedBy { it }
            .orEmpty()
}
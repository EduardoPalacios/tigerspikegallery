package com.tigerspike.domain

import com.tigerspike.domain.models.api.FlickrResponse
import io.reactivex.Single

/**
 * Interface adapter between domain and data modules.
 */
interface GalleryProvider {

    fun request(): Single<FlickrResponse>
    fun cache(list: List<String>)

}
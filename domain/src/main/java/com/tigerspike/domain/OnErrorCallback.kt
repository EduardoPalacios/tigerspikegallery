package com.tigerspike.domain

interface OnErrorCallback {

    fun onError()
    fun onNoContent()
}
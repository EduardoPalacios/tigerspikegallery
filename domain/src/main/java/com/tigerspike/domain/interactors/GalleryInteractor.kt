package com.tigerspike.domain.interactors

import com.tigerspike.domain.OnErrorCallback

interface GalleryInteractor {
    fun requestImages(callback: OnErrorCallback)
}
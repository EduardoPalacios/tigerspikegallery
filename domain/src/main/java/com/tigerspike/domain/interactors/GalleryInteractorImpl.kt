package com.tigerspike.domain.interactors

import com.tigerspike.domain.GalleryProvider
import com.tigerspike.domain.OnErrorCallback
import com.tigerspike.domain.mappers.FlickrResponseMapper
import com.tigerspike.domain.models.api.FlickrResponse
import javax.inject.Inject

class GalleryInteractorImpl @Inject constructor(private val provider: GalleryProvider, private val mapper: FlickrResponseMapper) : GalleryInteractor {
    override fun requestImages(callback: OnErrorCallback) {
        provider.request()
                .map { response: FlickrResponse -> mapper.map(response) }
                .subscribe(
                        { list ->
                            if (list.isNotEmpty()) {
                                provider.cache(list)
                            } else {
                                callback.onNoContent()
                            }
                        },
                        { _: Throwable? -> callback.onError() }
                )

    }

}
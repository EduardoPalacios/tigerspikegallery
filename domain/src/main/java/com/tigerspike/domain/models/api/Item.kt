package com.tigerspike.domain.models.api

import com.google.gson.annotations.SerializedName

class Item(
        @SerializedName("title")
        val title: String? = null,
        @SerializedName("link")
        val link: String? = null,
        @SerializedName("media")
        val media: Media? = null,
        @SerializedName("date_taken")
        val dateTaken: String? = null,
        @SerializedName("description")
        val description: String? = null,
        @SerializedName("published")
        val published: String? = null,
        @SerializedName("author")
        val author: String? = null,
        @SerializedName("author_id")
        val authorId: String? = null,
        @SerializedName("tags")
        val tags: String? = null
) {
    constructor(title: String?, dateTaken: String?, link: Media) : this(title, null, link, dateTaken, null, null, null, null, null)
}

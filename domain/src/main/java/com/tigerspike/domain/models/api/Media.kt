package com.tigerspike.domain.models.api

import com.google.gson.annotations.SerializedName

data class Media(
        @SerializedName("m")
        val mediaLink: String? = null
)

package com.tigerspike.domain.models.api

import com.google.gson.annotations.SerializedName

class FlickrResponse(
        @SerializedName("title")
        var title: String? = null,
        @SerializedName("link")
        var link: String? = null,
        @SerializedName("description")
        var description: String? = null,
        @SerializedName("modified")
        var modified: String? = null,
        @SerializedName("generator")
        var generator: String? = null,
        @SerializedName("items")
        var items: List<Item>? = null
) {
        constructor(items: List<Item>) : this(null, null, null,null,null,items)
}

package com.tigerspike.data.repository

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.tigerspike.data.api.service.ApiService
import com.tigerspike.data.api.service.RxScheduler
import com.tigerspike.data.room.gallery.GalleryDao
import com.tigerspike.domain.models.api.FlickrResponse
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks

class FlickrRepositoryTest {

    private val repository: FlickrRepository by lazy { FlickrRepository(apiService, dao, scheduler) }
    @Mock private lateinit var apiService: ApiService
    @Mock private lateinit var scheduler: RxScheduler
    @Mock private lateinit var dao: GalleryDao

    @Before
    fun setUp() {
        initMocks(this)
        whenever(scheduler.backgroundThread()).thenReturn(Schedulers.trampoline())
        whenever(scheduler.mainThread()).thenReturn(Schedulers.trampoline())
    }

    @Test
    fun shouldVerifyRequestIsMade() {
        whenever(apiService.getFlickrImages()).thenReturn(Single.just(FlickrResponse()))
        repository.request()
        verify(scheduler, times(1)).backgroundThread()
        verify(scheduler, times(1)).mainThread()
        verify(apiService, times(1)).getFlickrImages()
    }

    @Test
    fun shouldCacheListOfImage() {
        repository.cache(listOf("imageUrl"))
        verify(dao, times(1)).addImages(any())
    }

}
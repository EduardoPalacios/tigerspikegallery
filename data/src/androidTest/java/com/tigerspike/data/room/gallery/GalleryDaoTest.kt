package com.tigerspike.data.room.gallery

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.tigerspike.data.room.TigerRoomDatabase
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GalleryDaoTest {

    private lateinit var database: TigerRoomDatabase
    private lateinit var dao: GalleryDao

    @Before
    fun setUp() {
        // TODO improve testing suite with common package to avoid duplication in data and in app modules
        database = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), TigerRoomDatabase::class.java).build()
        dao = database.galleryDao()
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun shouldInsertListOfImageUrls() {
        val expected = GalleryEntity(listOf("imageUrl1", "imageUrl2", "imageUrl3"))
        dao.addImages(expected)
        val actual = dao.getEntity()
        assertNotNull(actual)
        assertEquals(expected, actual)
    }

}
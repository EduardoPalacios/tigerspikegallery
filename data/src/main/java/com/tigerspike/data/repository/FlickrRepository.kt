package com.tigerspike.data.repository

import com.tigerspike.data.api.service.ApiService
import com.tigerspike.data.api.service.RxScheduler
import com.tigerspike.data.room.gallery.GalleryDao
import com.tigerspike.data.room.gallery.GalleryEntity
import com.tigerspike.domain.GalleryProvider
import com.tigerspike.domain.models.api.FlickrResponse
import io.reactivex.Single
import javax.inject.Inject

class FlickrRepository @Inject constructor(
        private val apiService: ApiService,
        private val galleryDao: GalleryDao,
        private val schedulers: RxScheduler) : GalleryProvider {
    override fun request(): Single<FlickrResponse> {
        return apiService.getFlickrImages()
                .subscribeOn(schedulers.backgroundThread())
                .observeOn(schedulers.mainThread())
    }

    override fun cache(list: List<String>) {
        galleryDao.addImages(GalleryEntity(list))
    }

}
package com.tigerspike.data.room.gallery

import android.arch.persistence.room.*
import com.tigerspike.data.room.gallery.converters.StringListConverter

@Entity(tableName = GALLERY_ENTITY, indices = arrayOf(Index(value = COLUMN_IMAGE_URLS, unique = true)))
@TypeConverters(StringListConverter::class)
data class GalleryEntity(

        @ColumnInfo(name = COLUMN_IMAGE_URLS)
        val images: List<String>
) {
    @ColumnInfo(name = COLUMN_ID)
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}

const val GALLERY_ENTITY = "GalleryEntity"
const val COLUMN_IMAGE_URLS = "imageUrls"
const val COLUMN_ID = "_id"
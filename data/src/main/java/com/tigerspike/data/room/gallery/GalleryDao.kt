package com.tigerspike.data.room.gallery

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface GalleryDao {

    @Query(value = SELECT_ALL)
    fun getLiveDataEntity(): LiveData<List<GalleryEntity>>

    @Query(value = SELECT_ALL)
    fun getEntity(): List<GalleryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addImages(galleryEntity: GalleryEntity)
}

const val SELECT_ALL = "SELECT * FROM $GALLERY_ENTITY"
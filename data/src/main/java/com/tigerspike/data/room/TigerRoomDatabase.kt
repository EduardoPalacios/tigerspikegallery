package com.tigerspike.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.tigerspike.data.room.gallery.GalleryDao
import com.tigerspike.data.room.gallery.GalleryEntity

/**
 * Database class.
 *
 * Note that exportSchema is false. this should be configured to true and it would need to
 * have added some lines in the build file in order to generate a json with the database schema.
 * That is required to handle migrations.
 */
@Database(entities = arrayOf(GalleryEntity::class), version = DATABASE_VERSION, exportSchema = false)
abstract class TigerRoomDatabase : RoomDatabase(){

    abstract fun galleryDao() : GalleryDao
}

const val DATABASE_VERSION = 1
const val DATABASE_NAME = "tiger_room_db"
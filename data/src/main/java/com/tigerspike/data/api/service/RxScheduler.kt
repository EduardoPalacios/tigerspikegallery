package com.tigerspike.data.api.service

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * This interface provides an scheduler for Rx.
 * Useful for testing: mock the interface and return Schedulers.trampoline()
 */
interface RxScheduler {
    fun mainThread(): Scheduler = AndroidSchedulers.mainThread()
    fun backgroundThread(): Scheduler = Schedulers.io()
    fun backgroundThreadComputation(): Scheduler = Schedulers.computation()
}
package com.tigerspike.data.api.service

import com.tigerspike.domain.models.api.FlickrResponse
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {
    @GET(PUBLIC_PHOTOS)
    fun getFlickrImages(): Single<FlickrResponse>
}

const val PUBLIC_PHOTOS = "services/feeds/photos_public.gne/?format=json&nojsoncallback=1"
const val BASE_URL = "http://api.flickr.com/"
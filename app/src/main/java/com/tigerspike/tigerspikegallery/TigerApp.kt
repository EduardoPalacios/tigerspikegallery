package com.tigerspike.tigerspikegallery

import android.app.Application
import com.facebook.stetho.Stetho
import com.tigerspike.tigerspikegallery.presentation.di.components.ApplicationComponent
import com.tigerspike.tigerspikegallery.presentation.di.components.DaggerApplicationComponent
import com.tigerspike.tigerspikegallery.presentation.di.modules.ApplicationModule

class TigerApp : Application() {

    companion object {
        @JvmStatic lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(ApplicationModule((this))).build()
        Stetho.initializeWithDefaults(this)
    }
}
package com.tigerspike.tigerspikegallery.presentation.di.modules

import com.tigerspike.data.repository.FlickrRepository
import com.tigerspike.data.room.gallery.GalleryDao
import com.tigerspike.domain.GalleryProvider
import com.tigerspike.domain.interactors.GalleryInteractor
import com.tigerspike.domain.interactors.GalleryInteractorImpl
import com.tigerspike.tigerspikegallery.presentation.gallery.presenters.GalleryPresenter
import com.tigerspike.tigerspikegallery.presentation.gallery.presenters.GalleryPresenterImpl
import com.tigerspike.tigerspikegallery.presentation.gallery.viewmodels.GalleryViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class GalleryModule(val view: GalleryPresenterImpl.View) {

    @Provides
    fun provideGalleryPresenter(interactor: GalleryInteractorImpl): GalleryPresenter = GalleryPresenterImpl(view, interactor)

    @Provides
    fun provideGalleryInteractor(interactor: GalleryInteractorImpl): GalleryInteractor = interactor

    @Provides
    fun provideGalleryAdapter(flickrRepository: FlickrRepository): GalleryProvider = flickrRepository

    @Provides
    fun provideGalleryViewModelFactory(dao: GalleryDao): GalleryViewModelFactory = GalleryViewModelFactory(dao)


}
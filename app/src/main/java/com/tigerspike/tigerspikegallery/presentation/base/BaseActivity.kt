package com.tigerspike.tigerspikegallery.presentation.base

import android.support.v7.app.AppCompatActivity

/**
 *
 * Base class for common logic of the activities.
 *
 * At the beginning i considered to have a generic instance of the presenter as:
 * abstract class BaseActivity<T : BasePresenter<*>> : AppCompatActivity(), BaseView {
 *
 * But since we are injecting the presenter in the children of this class, we would have to do something
 * like this in the children:
 *      override val p: GalleryPresenter -> "p" is an abstract val that belongs to BaseActivity
 *      get() = presenter -> "presenter" is the result of @Inject lateinit var presenter: GalleryPresenter
 *
 * Probably the get() would be not need it, but anyway, i felt was a bit messy having the injection
 * of the presenter and the generic presenter for any activity together.
 *
 * It also implements required functionality of BasePresenterImpl.BaseView
 *
 * As this example is quite small, this class is empty but ready to use if need it.
 */
abstract class BaseActivity : AppCompatActivity(), BaseView
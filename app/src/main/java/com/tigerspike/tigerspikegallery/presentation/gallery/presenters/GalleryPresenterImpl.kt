package com.tigerspike.tigerspikegallery.presentation.gallery.presenters

import com.tigerspike.data.room.gallery.GalleryEntity
import com.tigerspike.domain.interactors.GalleryInteractor
import com.tigerspike.tigerspikegallery.presentation.base.BaseView
import javax.inject.Inject

/**
 * Implementation class of GalleryPresenter to handle the view flow and react after the user interaction.
 */
class GalleryPresenterImpl @Inject constructor(view: View, private val interactor: GalleryInteractor) : GalleryPresenter(view) {

    override fun load() {
        interactor.requestImages(this)
    }

    override fun onNoContent() {
        view.showEmptyScreen()
    }

    override fun onError() {
        view.showErrorScreen()
    }

    override fun findNewImages(newImages: List<GalleryEntity>, currentImages: MutableList<String>): List<String> {
        val allImages = newImages.flatMap { it.images }.distinctBy { it }.toMutableList()
        allImages.removeAll(currentImages)
        return allImages
    }

    /**
     * View interface of current presenter
     */
    interface View : BaseView
}
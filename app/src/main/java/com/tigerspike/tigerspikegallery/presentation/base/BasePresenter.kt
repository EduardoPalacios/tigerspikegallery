package com.tigerspike.tigerspikegallery.presentation.base

import com.tigerspike.domain.OnErrorCallback

/**
 * Base interface that will host the expected behaviour of the presenter in a view (Activity)
 * It has an instance of V that will represent the view (Activity)
 *
 * Note: this could be done with an interface. The difference is with this approach we can pass V
 * as a parameter in constructor and using an interface we will need a method where to set V
 * (e.g: fun attachView(v : V))
 */
abstract class BasePresenter<out V>(val view: V) : OnErrorCallback

package com.tigerspike.tigerspikegallery.presentation.gallery.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.tigerspike.data.room.gallery.GalleryDao
import com.tigerspike.data.room.gallery.GalleryEntity

class GalleryViewModel(dao: GalleryDao) : ViewModel() {

    val galleryLiveData: LiveData<List<GalleryEntity>> by lazy { dao.getLiveDataEntity() }
}
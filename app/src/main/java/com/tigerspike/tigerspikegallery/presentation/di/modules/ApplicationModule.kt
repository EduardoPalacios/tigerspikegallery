package com.tigerspike.tigerspikegallery.presentation.di.modules

import android.content.Context
import android.content.res.Resources
import com.tigerspike.data.api.service.RxScheduler
import com.tigerspike.tigerspikegallery.TigerApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: TigerApp) {

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideResources(): Resources = app.resources

    @Provides
    @Singleton
    fun provideSchedulers(): RxScheduler = object : RxScheduler {}
}
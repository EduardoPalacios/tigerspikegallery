package com.tigerspike.tigerspikegallery.presentation.gallery.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import com.tigerspike.tigerspikegallery.R
import com.tigerspike.tigerspikegallery.TigerApp
import com.tigerspike.tigerspikegallery.presentation.base.BaseActivity
import com.tigerspike.tigerspikegallery.presentation.di.modules.GalleryModule
import com.tigerspike.tigerspikegallery.presentation.gallery.adapters.GalleryAdapter
import com.tigerspike.tigerspikegallery.presentation.gallery.presenters.GalleryPresenter
import com.tigerspike.tigerspikegallery.presentation.gallery.presenters.GalleryPresenterImpl
import com.tigerspike.tigerspikegallery.presentation.gallery.viewmodels.GalleryViewModel
import com.tigerspike.tigerspikegallery.presentation.gallery.viewmodels.GalleryViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.empty_screen.*
import kotlinx.android.synthetic.main.error_screen.*
import java.util.*
import javax.inject.Inject

/**
 * This activity shows a list of images.
 */
class GalleryActivity : BaseActivity(), GalleryPresenterImpl.View {

    @Inject lateinit var presenter: GalleryPresenter
    @Inject lateinit var factory: GalleryViewModelFactory
    private val galleryAdapter = GalleryAdapter()

    /**
     * When the activity is instantiated we initialize dagger GalleryComponent
     */
    init {
        injectGalleryComponent()
    }

    private fun injectGalleryComponent() = TigerApp.applicationComponent.plus(GalleryModule(this)).inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initRecyclerView()
        if (savedInstanceState == null) {
            presenter.load()
            initGalleryViewModel()
        } else {
            val persistedImages = savedInstanceState.getStringArrayList(LIST_STATE)
            galleryAdapter.displayImages(persistedImages)
        }
    }

    private fun initRecyclerView() {
        recycler_view.apply {
            layoutManager = GridLayoutManager(this@GalleryActivity, 2)
            adapter = galleryAdapter
        }
    }

    private fun initGalleryViewModel() {
        val galleryViewModel = ViewModelProviders.of(this, factory).get(GalleryViewModel::class.java)
        galleryViewModel.galleryLiveData.observe(this, Observer { entity ->
            entity?.let {
                val images = presenter.findNewImages(it, galleryAdapter.list)
                if (images.isNotEmpty()) {
                    galleryAdapter.displayImages(images)
                }
            }
        })
    }

    override fun showErrorScreen() {
        gallery_container.visibility = GONE
        error_screen_container.visibility = VISIBLE
    }

    override fun showEmptyScreen() {
        gallery_container.visibility = GONE
        empty_screen_container.visibility = VISIBLE
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putMutableStringList(LIST_STATE, galleryAdapter.list)
    }
}

@Suppress("PrivatePropertyName")
const val LIST_STATE = "list_state"

fun Bundle.putMutableStringList(key: String, list: MutableList<String>) {
    putStringArrayList(key, list as ArrayList<String>)
}
package com.tigerspike.tigerspikegallery.presentation.gallery.adapters.viewholders

import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.gallery_view_holder.view.*

class GalleryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(imageUrl: String) {
        Picasso.with(itemView.context).load(imageUrl).centerInside().resize(500, 500).into(itemView.gallery_image_view)
    }
}
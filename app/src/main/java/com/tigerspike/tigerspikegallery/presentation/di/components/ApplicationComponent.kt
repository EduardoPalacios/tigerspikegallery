package com.tigerspike.tigerspikegallery.presentation.di.components

import com.tigerspike.tigerspikegallery.presentation.di.modules.ApiModule
import com.tigerspike.tigerspikegallery.presentation.di.modules.ApplicationModule
import com.tigerspike.tigerspikegallery.presentation.di.modules.GalleryModule
import com.tigerspike.tigerspikegallery.presentation.di.modules.RoomModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, ApiModule::class, RoomModule::class))
interface ApplicationComponent {

    fun plus(galleryModule: GalleryModule): GalleryComponent
}
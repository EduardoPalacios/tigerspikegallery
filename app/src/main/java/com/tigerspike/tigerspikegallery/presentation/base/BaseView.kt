package com.tigerspike.tigerspikegallery.presentation.base

/**
 * Base interface to link a presenter with its view (an Activity or Fragment)
 * With this abstraction we provide the needed instances from the view avoiding using the
 * Android framework.
 * This way we can mock easily the expected behaviour when the presenters needs to interact
 * with the view.
 */
interface BaseView {

    fun showErrorScreen()

    fun showEmptyScreen()
}
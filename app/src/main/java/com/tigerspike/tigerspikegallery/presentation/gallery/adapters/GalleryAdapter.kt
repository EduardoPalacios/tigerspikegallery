package com.tigerspike.tigerspikegallery.presentation.gallery.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.tigerspike.tigerspikegallery.R
import com.tigerspike.tigerspikegallery.presentation.gallery.adapters.viewholders.GalleryViewHolder

class GalleryAdapter : RecyclerView.Adapter<GalleryViewHolder>() {

    val list = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GalleryViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.gallery_view_holder, parent, false)
        return GalleryViewHolder(view)
    }

    override fun onBindViewHolder(holder: GalleryViewHolder?, position: Int) {
        holder?.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    fun displayImages(images: List<String>) {
        list.addAll(images)
        notifyDataSetChanged()
    }
}

package com.tigerspike.tigerspikegallery.presentation.di.components

import com.tigerspike.tigerspikegallery.presentation.di.modules.GalleryModule
import com.tigerspike.tigerspikegallery.presentation.di.scopes.PerActivity
import com.tigerspike.tigerspikegallery.presentation.gallery.activities.GalleryActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = arrayOf(GalleryModule::class))
interface GalleryComponent {
    fun inject(mainActivity: GalleryActivity)
}
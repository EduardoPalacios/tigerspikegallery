package com.tigerspike.tigerspikegallery.presentation.gallery.presenters

import com.tigerspike.data.room.gallery.GalleryEntity
import com.tigerspike.tigerspikegallery.presentation.base.BasePresenter

/**
 * Abstraction to expose the functionality that will be used in GalleryActivity
 */
abstract class GalleryPresenter(view: GalleryPresenterImpl.View) : BasePresenter<GalleryPresenterImpl.View>(view) {

    abstract fun load()
    abstract fun findNewImages(newImages: List<GalleryEntity>, currentImages: MutableList<String>) : List<String>
}
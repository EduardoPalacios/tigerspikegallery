package com.tigerspike.tigerspikegallery.presentation.di.modules

import com.google.gson.Gson
import com.tigerspike.data.api.service.ApiService
import com.tigerspike.data.api.service.BASE_URL
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideApiService(okHttpClient: OkHttpClient): ApiService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
            .create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideOkHttpClient(interceptor: () -> HttpLoggingInterceptor): OkHttpClient = OkHttpClient.Builder().apply {
        connectTimeout(30, TimeUnit.SECONDS)
        readTimeout(30, TimeUnit.SECONDS)
        writeTimeout(30, TimeUnit.SECONDS)
    }.addInterceptor(interceptor()).build()

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): () -> HttpLoggingInterceptor = { HttpLoggingInterceptor().setLevel((HttpLoggingInterceptor.Level.BODY)) }
}
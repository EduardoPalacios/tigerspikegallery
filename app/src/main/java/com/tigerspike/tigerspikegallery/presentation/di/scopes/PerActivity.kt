package com.tigerspike.tigerspikegallery.presentation.di.scopes

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity
package com.tigerspike.tigerspikegallery.presentation.gallery.viewmodels

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.tigerspike.data.room.gallery.GalleryDao
import javax.inject.Inject


class GalleryViewModelFactory @Inject constructor(private  val dao: GalleryDao) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GalleryViewModel::class.java)) {
            return GalleryViewModel(dao) as T
        }
        throw IllegalArgumentException("Unknown view model $modelClass")
    }
}
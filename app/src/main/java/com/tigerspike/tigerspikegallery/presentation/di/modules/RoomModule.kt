package com.tigerspike.tigerspikegallery.presentation.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.tigerspike.data.room.DATABASE_NAME
import com.tigerspike.data.room.TigerRoomDatabase
import com.tigerspike.data.room.gallery.GalleryDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @Singleton
    fun provideTigerRoom(context: Context): TigerRoomDatabase
            = Room.databaseBuilder(context, TigerRoomDatabase::class.java, DATABASE_NAME)
            .allowMainThreadQueries()
            .build()

    @Provides
    @Singleton
    fun provideGalleryDao(tigerRoomDatabase: TigerRoomDatabase): GalleryDao = tigerRoomDatabase.galleryDao()
}
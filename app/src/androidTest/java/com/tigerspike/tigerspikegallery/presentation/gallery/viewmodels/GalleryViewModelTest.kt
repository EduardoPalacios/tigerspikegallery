package com.tigerspike.tigerspikegallery.presentation.gallery.viewmodels

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.tigerspike.data.room.TigerRoomDatabase
import com.tigerspike.data.room.gallery.GalleryDao
import com.tigerspike.data.room.gallery.GalleryEntity
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class GalleryViewModelTest {

    private lateinit var database: TigerRoomDatabase
    private lateinit var dao: GalleryDao
    private val viewModel: GalleryViewModel by lazy { GalleryViewModel(dao) }

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), TigerRoomDatabase::class.java).build()
        dao = database.galleryDao()
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun shouldObserveDatabaseChanges() {
        viewModel.galleryLiveData.observeForever { entity: GalleryEntity? ->
            assertThat(entity?.images?.size, `is`(3))
        }
        dao.addImages(GalleryEntity(listOf("imageUrl1", "imageUrl2", "imageUrl3")))
    }
}
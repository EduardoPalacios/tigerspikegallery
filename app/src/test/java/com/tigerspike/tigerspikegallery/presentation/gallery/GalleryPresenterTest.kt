package com.tigerspike.tigerspikegallery.presentation.gallery

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.tigerspike.data.room.gallery.GalleryEntity
import com.tigerspike.domain.interactors.GalleryInteractor
import com.tigerspike.tigerspikegallery.presentation.gallery.presenters.GalleryPresenter
import com.tigerspike.tigerspikegallery.presentation.gallery.presenters.GalleryPresenterImpl
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks

class GalleryPresenterTest {

    private val presenter: GalleryPresenter by lazy { GalleryPresenterImpl(view, interactor) }
    @Mock lateinit var view: GalleryPresenterImpl.View
    @Mock lateinit var interactor: GalleryInteractor

    @Before
    fun setUp() {
        initMocks(this)
    }

    @Test
    fun shouldLoad() {
        presenter.load()
        verify(interactor, times(1)).requestImages(any())
    }

    @Test
    fun shouldShowEmptyView() {
        presenter.onNoContent()
        verify(view, times(1)).showEmptyScreen()
    }

    @Test
    fun shouldShowErrorScreen() {
        presenter.onError()
        verify(view, times(1)).showErrorScreen()
    }

    @Test
    fun shouldAddOnlyNewImages() {
        val newImages = listOf(GalleryEntity(listOf("image1", "image2", "image3")))
        val currentImages = mutableListOf("image1")
        val actual = presenter.findNewImages(newImages, currentImages)
        assertThat(actual, `is`(listOf("image2", "image3")))
    }

}